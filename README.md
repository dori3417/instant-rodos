# Instant-Rodos

Instant-RODOS is an Instant Up version of the real-time operating system RODOS.
Instant-RODOS presents some of the API and features of RODOS.

Information "classic" RODOS itself can be found here:
https://www.montenegros.de/sergio/rodos/index.html

Instant-RODOS is open source and released under the terms of the GPL V2. 
The license can be found in the build or here:
https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html





