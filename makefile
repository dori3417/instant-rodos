CPP=g++
CC=gcc
CFLAGS=-I.

DEPS = CommBuffer.h Fifo.h IdleTask.h InstantRodos.h LocalDefines.h Semaphore.h Thread.h ThreadHeap.h ThreadWithTimer.h TimeEvent.h TimeModel.h Timer.h TimerHeap.h TinyHeap.h Watcher.h rodos.h

OBJ = IdleTask.o InstantRodos.o Semaphore.o Thread.o ThreadHeap.o ThreadWithTimer.o TimeEvent.o TimeModel.o Timer.o TimerHeap.o Watcher.o main.o 
COBJ = printf.o


%.o: %.cpp $(DEPS)
	$(CPP) -c -o $@ $< $(CFLAGS)

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

instant-rodos: $(OBJ) $(COBJ)
	$(CPP) -o $@ $^ $(CFLAGS) 

clean:
	rm *.o
